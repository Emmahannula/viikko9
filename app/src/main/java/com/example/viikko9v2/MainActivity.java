package com.example.viikko9v2;

import android.os.StrictMode;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import org.xml.sax.SAXException;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;

import javax.xml.parsers.ParserConfigurationException;

import android.widget.TextView;

public class MainActivity extends AppCompatActivity {
    Spinner theatreSpinner;
    Spinner movieSpinner;
    program program = new program();
    EditText paiva;
    TextView text;
    RecyclerView Rview;
    EditText start;
    EditText end;
    ArrayList leffat;
    TextAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        leffat = null;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        theatreSpinner = (Spinner) findViewById(R.id.theatreSpinner);
        movieSpinner = (Spinner) findViewById(R.id.movieSpinner);
        paiva = (EditText) findViewById(R.id.editText);
        start = (EditText) findViewById(R.id.start);
        end = (EditText) findViewById(R.id.end);
        program.defaultDate();
        setRecyclerView(leffat);
        try {
            readXMLTheaters();
            readXMLMovies();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }


    }

    public void readXMLTheaters() throws ParserConfigurationException, IOException, SAXException {
        program.readXMLfile();
        addToSpinnerTheatres(program.lista);
    }

    public void addToSpinnerTheatres(ArrayList lista) {
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, lista);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        theatreSpinner.setAdapter(dataAdapter);
    }

    public void addToSpinnerMovies(ArrayList lista) {
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, lista);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        movieSpinner.setAdapter(dataAdapter);
    }


    public void showMovies(View v) throws ParserConfigurationException, IOException, SAXException, ParseException {
        if (leffat != null) {
            leffat.clear();
        }
        adapter.notifyDataSetChanged();
        if (theatreSpinner.getSelectedItemPosition()== 0) {
            findTheaters();
        }
        else if (movieSpinner.getSelectedItemPosition()==0) {
           program.findMovies(paiva, theatreSpinner, text);
           leffat = program.findShows(start, end);
           setRecyclerView(leffat);
        }
        else {
            if (program.TheOne != null) {
                program.TheOne.clear();
            }
            adapter.notifyDataSetChanged();
            program.findTheOne(movieSpinner, theatreSpinner, start, end);
            setRecyclerView(program.TheOne);
        }
    }

    public void setRecyclerView(ArrayList leffat) {
        Rview = (RecyclerView) findViewById(R.id.recycleView);
        Rview.setHasFixedSize(true);
        Rview.setLayoutManager((new LinearLayoutManager(this)));
        adapter = new TextAdapter(leffat);
        Rview.setAdapter(adapter);
    }


    public void readXMLMovies() throws ParserConfigurationException, SAXException, IOException {
        program.readXMLMovies();
        addToSpinnerMovies(program.allMovies);
    }

    public void findTheaters() throws IOException, SAXException, ParserConfigurationException, ParseException {
        if (leffat != null) {
            leffat.clear();
        }
        adapter.notifyDataSetChanged();
        program.findTheaters(movieSpinner);
        leffat = program.findShows2(start, end);
        setRecyclerView(leffat);
    }

}
