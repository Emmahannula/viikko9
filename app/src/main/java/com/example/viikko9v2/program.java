package com.example.viikko9v2;


import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.lang.reflect.Array;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.Set;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

public class program {
    ArrayList<String> lista = new ArrayList<>();
    ArrayList<String> kellonajat = new ArrayList<>();
    ArrayList<String> leffat= new ArrayList<>();
    ArrayList<String> halutut = new ArrayList<>();
    ArrayList<String> allMovies = new ArrayList<>();
    ArrayList<String> allTheaters = new ArrayList<>();
    ArrayList<String> TheOne = new ArrayList<>();

    Theater teatteri;
    String item;
    SimpleDateFormat parser = new SimpleDateFormat("HH:mm");
    String pvm;

//Haetaan elokuvia teatterin perustella

    public void readXMLfile() throws IOException, SAXException, ParserConfigurationException {
        String url = "https://www.finnkino.fi/xml/TheatreAreas/";
        DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        Document doc = builder.parse(url);
        doc.getDocumentElement().normalize();
        NodeList list = doc.getDocumentElement().getElementsByTagName("TheatreArea");
        parseList(list);
    }

    public void parseList(NodeList list) {
        for (int i = 0; i<list.getLength(); i++) {
            Node node = list.item(i);

            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) node;
                String id = element.getElementsByTagName("ID").item(0).getTextContent();
                String name = element.getElementsByTagName("Name").item(0).getTextContent();
                teatteri = new Theater(name, id);
                lista.add(name);
                //System.out.println(id +" "+ name);
            }
        }
    }
    public void findMovies(EditText paiva, Spinner spinner, TextView text) throws ParserConfigurationException, IOException, SAXException {
        item = String.valueOf(spinner.getSelectedItem());
        pvm = paiva.getText().toString();
        String id = findItemOnList(item);
        //System.out.println(pvm);
        String url = "https://www.finnkino.fi/xml/Schedule/?area="+id+"&dt="+pvm;
        DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        Document doc = builder.parse(url);
        doc.getDocumentElement().normalize();
        NodeList list2 = doc.getDocumentElement().getElementsByTagName("Show");
        listShows(list2);
    }

    public void listShows(NodeList list2) {
        for (int i = 0; i < list2.getLength(); i++) {
            Node node = list2.item(i);

            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) node;
                String nimi = element.getElementsByTagName("Title").item(0).getTextContent();
                String aika = element.getElementsByTagName("dttmShowStart").item(0).getTextContent();
                leffat.add(nimi);
                kellonajat.add(aika);
            }
        }
    }



    public ArrayList findShows(EditText start, EditText end) throws ParseException {
        if (start.getText().toString().equals("") || end.getText().toString().equals("")) {
            return leffat;
        }
        else {
            Date chosedStart = parser.parse(start.getText().toString());
            Date chosedEnd = parser.parse(end.getText().toString());
            for (int i = 0; i < kellonajat.size(); i++) {
                String[] time = kellonajat.get(i).split("T");
                Date startTime = parser.parse(time[1]);
                if (startTime.after(chosedStart) && startTime.before(chosedEnd)) {
                    halutut.add(time[1] + " " +leffat.get(i));
                }
            }
            return halutut;
        }
    }

//Haetaan teattereita elokuvan perusteella

    public void readXMLMovies() throws IOException, SAXException, ParserConfigurationException {
        String url = "https://www.finnkino.fi/xml/Schedule/?dt="+pvm;
        DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        Document doc = builder.parse(url);
        doc.getDocumentElement().normalize();
        NodeList list = doc.getDocumentElement().getElementsByTagName("Show");
        findMoviesXML(list);
    }

    //Luo listan spinneriä varten

    public void findMoviesXML(NodeList list){
        allMovies.add("Valitse elokuva:");
        for (int i = 0; i<list.getLength(); i++) {
            Node node = list.item(i);

            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) node;
                String name = element.getElementsByTagName("Title").item(0).getTextContent();
                allMovies.add(name);
            }
        }
        Set<String> set = new LinkedHashSet<>();
        set.addAll(allMovies);
        allMovies.clear();
        allMovies.addAll(set);

    }

    public void findTheaters(Spinner spinner) throws ParserConfigurationException, IOException, SAXException, ParseException {
        item = String.valueOf(spinner.getSelectedItem());
        String url = "https://www.finnkino.fi/xml/Schedule/?dt="+pvm;
        DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        Document doc = builder.parse(url);
        doc.getDocumentElement().normalize();
        NodeList list3 = doc.getDocumentElement().getElementsByTagName("Show");
        parseShowList(list3);
    }

    public void parseShowList(NodeList list3) throws ParseException {
        kellonajat.clear();
        allTheaters.add("***"+item+"***");
        for (int i = 0; i < list3.getLength(); i++) {
            Node node = list3.item(i);

            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) node;
                String theater = element.getElementsByTagName("Theatre").item(0).getTextContent();
                String time = element.getElementsByTagName("dttmShowStart").item(0).getTextContent();
                String nameOfShow = element.getElementsByTagName("Title").item(0).getTextContent();
                String[] timeSplit = time.split("T");
                if (item.equals(nameOfShow)) {
                    allTheaters.add(timeSplit[1] + " " + theater);
                    kellonajat.add(timeSplit[1]);
                }
            }
        }
    }

    public ArrayList findShows2(EditText start, EditText end) throws ParseException {
        halutut.clear();
        if (start.getText().toString().equals("") || end.getText().toString().equals("")) {
            return allTheaters;
        }
        else {
            Date chosedStart = parser.parse(start.getText().toString());
            Date chosedEnd = parser.parse(end.getText().toString());

            for (int i = 0; i < kellonajat.size(); i++) {
                Date startTime = parser.parse(kellonajat.get(i));
                if (startTime.after(chosedStart) && startTime.before(chosedEnd)) {
                    halutut.add(allTheaters.get(i));
                }
            }
            return halutut;
        }
    }

    public void findTheOne(Spinner movie, Spinner theatre, EditText start, EditText end) throws IOException, SAXException, ParserConfigurationException, ParseException {
        String movieName = String.valueOf(movie.getSelectedItem());
        String theatreName = String.valueOf(theatre.getSelectedItem());
        String id = findItemOnList(item);

        String url = "https://www.finnkino.fi/xml/Schedule/?area="+id+"&dt="+pvm;
        DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        Document doc = builder.parse(url);
        doc.getDocumentElement().normalize();
        NodeList list3 = doc.getDocumentElement().getElementsByTagName("Show");
        TheOne.add(movieName+ ": "+theatreName );
        for (int i = 0; i < list3.getLength(); i++) {
            Node node = list3.item(i);

            if (node.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) node;
                String time = element.getElementsByTagName("dttmShowStart").item(0).getTextContent();
                String nameOfShow = element.getElementsByTagName("Title").item(0).getTextContent();
                String[] timeSplit = time.split("T");
                if (movieName.equals(nameOfShow)) {
                    TheOne.add(timeSplit[1]);
                }
            }
        }
        if (start.getText().toString().equals("") || end.getText().toString().equals("")) {
            ;
        }
        else {
                if (start.getText().toString().equals("") || end.getText().toString().equals("")) {
                } else {
                    for (int i = 1; i < TheOne.size(); i++) {
                        Date chosedStart = parser.parse(start.getText().toString());
                        Date chosedEnd = parser.parse(end.getText().toString());
                            Date startTime = parser.parse(TheOne.get(i));
                            if (startTime.after(chosedStart) && startTime.before(chosedEnd)) {
                            }
                            else {
                                TheOne.remove(i);
                    }
                }
            }
        }
        if (TheOne.size()==1) {
            TheOne.add("Elokuvaa ei ole kyseisessä teatterissa");
        }
    }

    public void defaultDate() {
        DateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        Date date = new Date();
        pvm = dateFormat.format(date);
    }

    public String findItemOnList(String item) {
        String id = teatteri.returnID();
        return id;
    }
}
