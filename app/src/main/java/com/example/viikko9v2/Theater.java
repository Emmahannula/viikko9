package com.example.viikko9v2;

public class Theater {
    String name;
    String id;

    public Theater (String name, String id) {
        this.name = name;
        this.id = id;
    }


    public String returnID() {
        return id;
    }

    public String returnName() {
        return name;
    }
}
